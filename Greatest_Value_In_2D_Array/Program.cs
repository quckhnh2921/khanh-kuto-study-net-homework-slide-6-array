﻿int[,] arr2D =
{
    {3,8,1,2 },
    {3,9,1,2}
};
int greatestElement = 0;
for(int i = 0; i < arr2D.GetLength(0); i++)
{
    for(int j =0; j < arr2D.GetLength(1); j++)
    {
        if (arr2D[i, j] > greatestElement)
        {
            greatestElement = arr2D[i,j];
        }
    }
}
Console.WriteLine(greatestElement);